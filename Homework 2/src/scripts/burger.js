const burgerButton = document.querySelector('.container__burger');
const burgerMenu = document.querySelector('.container__menu')


burgerButton.addEventListener('click', function () {
    const imageBurgetButton = burgerButton.querySelector('img');
    if (burgerMenu.style.visibility === 'hidden') {
        imageBurgetButton.setAttribute('src', 'images/icons/icon-burger-close.svg')
        burgerMenu.style.visibility = 'visible';
    } else {
        imageBurgetButton.setAttribute('src', 'images/icons/icon-burger.svg')
        burgerMenu.style.visibility = 'hidden';
    }
}) 